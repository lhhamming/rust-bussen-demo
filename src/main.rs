use std::{vec, io};
use rand::Rng;

/*
    Generates an basic card deck.
*/
fn build_deck() -> Vec<&'static str>{
    let deck_settings = settings();
    //JR => Joker rood
    //JZ => Joker Zwart
    //TODO: Convert array to Vector for variable sizes. Array have a set size. Vectors dont.
    let mut cards = vec!["S1","S2","S3","S4","S5","S6","S7","S8","S9","S10","SB","SV","SK",
                         "H1","H2","H3","H4","H5","H6","H7","H8","H9","H10","HB","HV","HK",
                         "R1","R2","R3","R4","R5","R6","R7","R8","R9","R10","RB","RV","RK",
                         "K1","K2","K3","K4","K5","K6","K7","K8","K9","K10","KB","KV","KK"];

    if deck_settings[1] {
        cards.push("JZ");
        cards.push("JR");
    }

    cards
}
/*
    Converts a card to an number
*/
fn card_to_number(card : &str) -> i16 {
    //TODO implement rules.
    let char_card = card.chars().nth(1).unwrap();
    println!("Card: {:?}", char_card);
    let is_figure_card =  if char_card == 'B' || char_card == 'V' || char_card == 'H' || char_card == 'Z' || char_card == 'R' || char_card == 'K' { true } else { false };
    let mut card_value = -1;
    if is_figure_card {
        //Card is a not a figure card so quite easy.
        if char_card == 'V' {
            let woman_lowest = true; // TODO read from settings
            card_value = if woman_lowest == true { 0 } else { 12 }
        }

        if char_card == 'H' {
            card_value = 13;
        }

        if char_card == 'K' {
            card_value = 12;
        }

        if char_card == 'B' {
            card_value = 11;
        }



        if char_card == 'R' || char_card == 'Z' {
            card_value = -1;
        }
    } else {
        card_value = char_card.to_digit(10).unwrap() as i16;
    }

    card_value
}

/*
    Settings for the game. Read the README what each of these does / should do
*/
fn settings() -> [bool; 4] {
    let mut arr_settings = [true,true,false,false];
    //TODO: QUEEN / VROUW laagste kaart?
    //TODO: Have Jokers?
    //TODO: If jokers => Shotjes of niet
    //TODO: If jokers => Mag je hem opgooien ja / nee
    //TODO: Presets => Pimsen, Default
    arr_settings[0] = true; // Queen / Woman lowest card;
    arr_settings[1] = true; // Jokers in the game?
    arr_settings[2] = true; //Jokers are shots.
    arr_settings[3] = true; //Jokers may be thrown in the bus.

    arr_settings
}

fn get_card(mut deck : Vec<&'static str>) -> (&'static str, Vec<&'static str>){
    //Choose random index to remove a card from
    let mut rng = rand::thread_rng();
    let chosen_index = rng.gen_range(0..deck.len());
    let chosen_card = deck[chosen_index];

    //Remove the card from the deck
    deck.retain(|&element| element != chosen_card);
    println!("Chosen card is {}", chosen_card);
    //Return both the chosen card and the deck.
    return (chosen_card, deck)
}

//Gives 1 card
fn give_card(mut deck : Vec<&'static str>, hand : &mut[&str; 4], step : i32) -> Vec<&'static str>{
    let (chosen_card, deck) = get_card(deck);

    //place the card into the hand
    hand[step as usize] = chosen_card;

    // Return the deck so it can be remutated
    deck
}

fn generate_player_hands(x: i32) -> Vec<[&'static str; 4]> {
    let mut players = Vec::new();

    for _i in 0 .. x {
        players.push(["","","",""]);
    }

    players
}

fn show_has_to_drink(x: bool) {
    if x {
        println!("You have to drink!");
    } else {
        println!("You don't have to drink!");
    }
}

fn player_input(text_to_show: &str, true_value : &str, false_value : &str) -> bool {
    let mut result = false;
    println!("{:?}",text_to_show);
    print!("Input {:?} or {:?}\n", true_value, false_value);
    let mut input = String::new();
    io::stdin()
        .read_line(&mut input)
        .expect("Failed to readline");
    input = input.trim().to_lowercase().to_string();

    result = if input == true_value { true } else { false };

    result
}

fn player_has_rainbow(player_choice: bool, hand: [&str; 4]){
    let mut has_to_drink = true;
    //we have to check the sleeves
    let mut sleeves = Vec::new();
    let required_sleeves = vec!['S','H','R','K'];
    for hand_iter in 0 .. hand.len() {
        let card = hand[hand_iter as usize];
        sleeves.push(card.chars().next().unwrap());
    }
    //Rainbow = S,H,R,K in any order
    if required_sleeves.iter().all(|sleeve| sleeves.contains(sleeve)) {
        if player_choice {
            has_to_drink = false;
        }
    }

    show_has_to_drink(has_to_drink);
}

/*
    card_one = First card in player hand
    card_two = Second card in player hand
    compare_card = Card to be checked if it is in between card one and card two.
*/
fn inbetween_step(player_choice: bool, card_one : &str, card_two : &str, compare_card : &str){
    //Player choice is meant to be inbetween yes or no
    let mut is_inbetween = false;
    let mut has_to_drink = false;

    let card_one_value = card_to_number(card_one);
    let card_two_value = card_to_number(card_two);
    let compare_card_value = card_to_number(compare_card);

    println!("Card one value: {:?}", card_one_value);
    println!("Card two value: {:?}", card_two_value);
    println!("compare card value: {:?}", compare_card_value);


    if card_one_value > compare_card_value && compare_card_value < card_two_value {
        is_inbetween = true
    } else if card_two_value > compare_card_value && compare_card_value < card_one_value {
        is_inbetween = true
    }

    if player_choice == is_inbetween {
        has_to_drink = true
    }

    show_has_to_drink(has_to_drink);
}

fn higher_lower_step(player_choice: bool, card: &str ,drawn_card: &str){
    let mut card_value = card_to_number(card);
    let mut drawn_card_value = card_to_number(drawn_card);
    let mut has_to_drink = false;

    if player_choice && drawn_card_value >! card_value {
       //Player said higher but it aint so
        has_to_drink = true;
    }
    if !player_choice && drawn_card_value > card_value {
        //player didnt said it was higher, but it was
        has_to_drink = true;
    }
    show_has_to_drink(has_to_drink);
}

//Red or black step
fn red_or_black_step(player_choice: bool, card : &str) {
    //If player says they have red its true otherwise false
    let mut has_to_drink = false;
    let card_sleeve = card.chars().next().unwrap();
    // H = Harten / Hearts & R = Ruiten / Diamonds both are red cards
    let result = if card_sleeve == 'H'  || card_sleeve == 'R' { true } else { false };

    if player_choice != result {
        has_to_drink = true;
    }
    show_has_to_drink(has_to_drink);

}

//Determine what step to do
fn game_step(current_step: i32, hand : [&'static str; 4]) {
    match current_step {
        /* Real play
        0 => red_or_black_step(player_input("Will you get a red or black card?", "Red", "Black"), hand[0]), //Red or black
        1 => higher_lower_step(player_input("Will the next card be higher or lower? then your previous card?", "Yes", "No"), hand[0], hand[1]), //Higher lower
        2 => inbetween_step(player_input("Will the next drawn card be inbetween the current card values?", "Yes", "No"), hand[0], hand[1], hand[2]), //Inbeteween
        3 => player_has_rainbow(player_input("Will the hand be made up of every sleeve?", "Yes", "No"), hand), //Rainbow
        _ => println!("Default, idk how you ended up here")
        */ //Auto play
        0 => red_or_black_step(true, hand[0]), //Red or black
        1 => higher_lower_step(true, hand[0], hand[1]), //Higher lower
        2 => inbetween_step(true, hand[0], hand[1], hand[2]), //Inbeteween
        3 => player_has_rainbow(true, hand), //Rainbow
        _ => println!("Default, idk how you ended up here")
    }
}

fn start_pyramid_phase(mut deck : Vec<&'static str>, mut player_hands : Vec<[&str; 4]>){
    //Set hight
    let pyramid_height = 6;
    //Total amount of cards (should be calculated with the height -1)
    let pyramid_total_cards = 15; //5+4+3+2+1
    //Show empty pyramid.
    generate_ascii_pyramid(pyramid_height);

    //Prepare pyramid
    let pyramid_deck = prepare_pyramid(16, deck);

    //Turn over cards
    for current_card in 1..16 {
        turn_over_card(pyramid_height, pyramid_deck.clone(), current_card);
    }
}

fn prepare_pyramid(total_cards: i32, mut deck : Vec<&'static str> ) ->  Vec<&'static str> {
    let mut pyramid = Vec::new();
    for i in 0..total_cards {
        let (chosen_card, deck) = get_card(deck.clone());
        pyramid.push(chosen_card)
    }

    pyramid
}

fn turn_over_card(rows: i32, pyramid_deck : Vec<&'static str> , current_card : i32 ) {
    let mut space_between = rows + 1;
    let mut card_iter = 1;
    for i in 0 .. rows {
        /*
        j=0; j < space_between; j++
        j-0; j <= i; j++
        */
        for j in 0..space_between {
            print!(" ")
        }

        for j in 0..i {
            let card = if card_iter <= current_card {pyramid_deck[card_iter as usize]} else {"X"};
            card_iter = card_iter + 1;
            print!("[{}]",card)
        }
        space_between = space_between -1;
        println!();
    }
    println!();
}

/*
The deck in this case is the left over cards from the deck.
Height is the height of the pyramid
*/
fn generate_ascii_pyramid(rows : i32) {
    let mut space_between = rows + 1;
    for i in 0 .. rows {
        /*
        j=0; j < space_between; j++
        j-0; j <= i; j++
        */

    for j in 0..space_between {
        print!(" ")
    }

    for j in 0..i {
        print!("[X]")
    }
    space_between = space_between -1;
    println!();
    }
    println!();

}

fn main() {
    let mut deck = build_deck();
    //Player selection
    let amount_of_players = 4;
    let mut players = generate_player_hands(amount_of_players);

    //Loop through what step of the game we are:
    //Step 1 : Red or black
    //Step 2 : Higher or lower than the previous card
    //Step 3 : Is the given card going to be between the cards in the hand OR not
    //Step 4 : Rainbow or no | maybe add red- and blackbow option

    //Max steps for bussen
    let max_steps = 4;  //Bussen always has the 4 steps
    for step in 0 .. max_steps {
        println!("Step : {}", step);
        for player_iter in 0..amount_of_players {
            println!("Current Player {:?}", player_iter);
            let mut current_player = players[player_iter as usize];
            //Draw a card here
            deck = give_card(deck, &mut current_player, step);
            println!("Hand: {:?}", current_player);
            game_step(step, current_player);
            println!("Hand after game step: {:?}", current_player);
            players[player_iter as usize] = current_player;
            // Rust apparently made a clone of the memory instead of setting it. Meaning that we didn't save the current hand.
            println!("----------------------------------");
        }
    }
    println!("Finished dealing hands! Creating the pyramid.");
    start_pyramid_phase(deck, players)
}