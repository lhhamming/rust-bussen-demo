# Rust Bussen
An implementation of bussen
Developed by Luuk Hamming


# What is bussen
Bussen is an infamous dutch drinking game that is played with a deck of cards.

## The rules

Bussen can be modified to your liking!

But the general rules are as follows:

### Losing and winning

If you win you dont have to do anything.
If you lose you have to take a sip of your drink.

The winning statement can be modified how ever!
Lets say you win you can also point to someone who has to take an sip of their drink!

### The goal

Be the person to have the least amount of cards in your hand so you don't go into the bus!

### Build up

- Your first card is either red or black
- Then you have to say if the next card is going to be lower or higher then the card you are currently holding
- Does the third card land in-between or outside of the range of number you currently have
    - E.G. If you have an 5 and a 10 and you said in-between and the dealer deals you an 3 this means you have lost.
    did you get a 6,7,8 or an 9 this means that you would have won.
- At the end you get the question if you already have the sleeve of the card or not (Diamonds, Hearts, Clubs, Spades).
    - bonus! If you say: "I will get an rainbow" this significies that you already have 1 of each sleeve. If you get the rainbow you win and everyone else loses and if you dont get the rainbow only you lose!

### The Pyramid (The fun part)

The pyramid is made differently by every person. but in general it will be build like this:

```
[card] [card] [card] [card] [card]
    [card] [card] [card] [card]
      [card] [card] [card]
         [card] [card]
             [card]
```

These cards will be upside down! so you dont know what card will be put there!
One card at an time will be flipped for the pyramid from top to bottom or bottom to top, how ever you like it!

But, be warned. after every step you take into building scaling this pyramid the amount you have to drink is plussed by 1 and the final row will be shots (or not, settings will be settings!)

When an card is flipped all of the other players that have this card can put their card on top of the card in the pyramid and dispose of their cards.
When you lay an card down, you can point to someone who has to drink. But dont make to many enemies otherwise you better prepare to drink a lot yourself!

This will go on until the pyramid is finished.

### The bus (if you lost, GL, if you won enjoy!)
At the end of the game of the pyramid the player with the least amounts of cards will be going into the bus!

The length of the bus is either pre-determined or the more fun way, the player will draw a card and then "throw it", if it lands open they have an open bus (easier), if it lands closed the bus will be closed (harder).

The card drawn determines the length of the bus (Minimum of 5 but can be changed in the settings)

When the bus is open, this means the player only has to guess if the next card is going to be higher or lower than the current one on the table.

When the bus is closed, this means the player has to guess: is the card going to be red or black and (optionally) say if the next thrown card is going to be higher or lower then the card that just turned around.

when the player loses they go back to the beginning of the bus.
when they lose they either drink x amount of sips depending on where they ended or they just take 1 sip.

Do this until the bus is finished!

#### Stalemate
When you have 2 people with the same amount of cards left over any of the following can be done (and will be options / settings):

- The person with the highest card goes into the bus
- Both people will pull a card and 1 random card will be chosen. then someone will say that the (Closest / furthest) card has to go into the bus
- Maybe more idea's?

### Ending
Start again :)


## Presets

Pimsen:

Description: you die

- Jokers are in the game
    - When given a joker everyone has to take a drink
    - Jokers may not be put on the pyramid
    - When a joker is turned over in the pyramid everyone has to drink x amount of sips of the jokers position
    - When a joker is turned over in the bus everyone has to drink x amount of sips of the jokers position & the players guess still counts to that position in the bus. 
- Queen's are the lowest cards
- Ace's are always the highest cards
- Checkpoints don't exist unless the bus is un-even (autism)
- When you lose on the last card its always a shot
- The deck always has an high amount of cards
- The pyramid will go from bottom to top
    - Row 1 is 1 sip, Row 2 is 2 etc...
    - Row 5 is all shots
- Red bow's (Thijmen bow) and Black bow's are also included
- You can point at people who to drink during the dealing of the cards


Default:
- Jokers are not in the game
- Cards have their normal values
- Every lose is 1 sip
- You cant point to someone to drink
- the bus has an max of 10
- the bus has an checkpoint after 5
- The pyramid will go from top to bottom.
    - Row 5 is 1 sip, Row 4 is 2 etc..
    - Row 1 is 1 shot

# Current progress
- Hand Dealing 
    - Computer can generate card deck ✅
    - Computer can 'pick a card' ✅
    - Program includes the 4 types of 'dealing' strategies
        - Red or black ✅
        - Higher Lower ✅
        - Inbetween or outside ✅
        - Rainbow step ✅
    - Pyramid 
        - Building ✅
        - Card flipping ✅
        - Card 'determinig' ✅
        - Players can 'throw' card on the pyramid ⛔
        - Players can determine who has to drink ⛔
        - Computer can determine who goes into the 'bus'
    - The bus
        - Can be created ⛔
            - Can be created dynamically ⛔
        - Determine ⛔
            - Open or closed bus ⛔
            - Red or black ⛔
            - Jokers? ⛔
            - Right or Wrong ⛔

## Rust progress
    - Has Vectors [Arrays] ✅
    - Function Oriented ✅
    - Implements classes ⛔